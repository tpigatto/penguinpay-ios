# PenguinPay - iOS

PenguinPay is an imaginary money transmitter that helps people send money from the US to several countries in Africa. This iOS app helps people from Binaria send money to their beloved ones in Kenya, Nigeria, Tanzania and Uganda.
___
## Features

  - Send money from US Dollars to local currency
  - Select which country you want to send money to
  - Use only binary numbers for amount of money sent (_We know Binarian people hate apps that use decimals_)
  - See exchanged value in Binary
___
## What's missing? (To do)
- Binary phone (how binary people can type and read a phone that is not in binary 🤔)
- Load api data before sending the money (api only each hour)
- Specific user data error messages
- Better api error handling
- Better UI
- Unit Tests

___

## Additional Info

- Built in xcode 13.0 with Apple Swift version 5.5
- Exchange rates provided by https://openexchangerates.org free api tier
