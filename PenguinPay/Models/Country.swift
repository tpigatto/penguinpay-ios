//
//  Country.swift
//  PenguinPay
//
//  Created by Tiago Pigatto Lenza on 25/11/21.
//

import Foundation

struct Country {
    
    var name: String
    var currencyAbbreviation: String
    var phonePrefix: String
    var flag: String
    var digitsAfterPrefix: Int
    
    
    var flagAndDigits: String {
        return flag + phonePrefix
    }
    
    init( name: String, currencyAbbreviation: String, phonePrefix: String, flag: String, digitsAfterPrefix: Int) {
        self.name = name
        self.currencyAbbreviation = currencyAbbreviation
        self.phonePrefix = phonePrefix
        self.flag = flag
        self.digitsAfterPrefix = digitsAfterPrefix
    }
}

struct CountriesData {

    let kenya = Country(name: "Kenya", currencyAbbreviation: "KES", phonePrefix: "+254", flag: "🇰🇪", digitsAfterPrefix: 9)
    let nigeria = Country(name: "Nigeria", currencyAbbreviation: "NGN", phonePrefix: "+234", flag: "🇳🇬", digitsAfterPrefix: 7)
    let tanzania = Country(name: "Tanzania", currencyAbbreviation: "TZS", phonePrefix: "+255", flag: "🇹🇿", digitsAfterPrefix: 9)
    let uganda = Country(name: "Uganda", currencyAbbreviation: "UGX", phonePrefix: "+256", flag: "🇺🇬", digitsAfterPrefix: 7)
    
    var countries: [Country]
    
    init () {
        self.countries = [kenya, nigeria, tanzania, uganda]
    }
    
}
