//
//  Coordinator.swift
//  PenguinPay
//
//  Created by Tiago Pigatto Lenza on 25/11/21.
//

import UIKit

class AppCoordinator {
    
    private var presenter: UINavigationController
    private var window: UIWindow
    private weak var appDelegate: AppDelegate?
    
    init(appDelegate: AppDelegate, window: UIWindow) {
        self.appDelegate = appDelegate
        self.window = window
        self.presenter = UINavigationController()
    }
    
    func start() {
        let viewController = SendPaymentViewController()
        self.presenter = UINavigationController(rootViewController: viewController)
        self.appDelegate?.window = self.window
        window.rootViewController = presenter
        window.makeKeyAndVisible()
    }
}
