//
//  API.swift
//  PenguinPay
//
//  Created by Tiago Pigatto Lenza on 25/11/21.
//

import Foundation

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

class ExchangeRateAPIClient {
    
    private let api_app_id: String = "b2013e97f09441a1a0b1d9cbe131ca32"
    private let session: URLSession
    private let endpointString = String("https://openexchangerates.org/api/latest.json")
    
    
    init(session: URLSession = URLSession(configuration: .default)){
        self.session = session
    }
    
    
    func fetch(completion: @escaping ( (Result<Rates>) -> Void) ){
        
        let queryItems = [URLQueryItem(name: "app_id", value: api_app_id)]
        
        guard var endpointURLComponents = URLComponents(string: endpointString) else {
            fatalError("Could not create URLComponent from endpoint string")
        }
        
        endpointURLComponents.queryItems = queryItems
        
        guard let requestURL = endpointURLComponents.url else {
            fatalError("Could not create URL from components")
        }
        
        let request = URLRequest(url: requestURL)
        
        session.dataTask(with: request) { data, response, error in
            let decoder = JSONDecoder()
            if let data = data {
                
                do{
                    let rates = try decoder.decode(Rates.self, from: data)
                    DispatchQueue.main.async {
                        completion(.success(rates))
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(.failure(error))
                    }
                }
                
            }else if let error = error {
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            }
            
        }.resume()
    }
    
}
