//
//  SendPaymentViewModel.swift
//  PenguinPay
//
//  Created by Tiago Pigatto Lenza on 25/11/21.
//


import UIKit

class SendPaymentViewModel {
    
    struct Constants {
        static let maxValueWarn = "Max value reached"
        static let receiveValueText = "Your friend will receive:"
        static let alertTitleSuccess = "Success!"
        static let alertTitleFailure = "Oops, something went wrong"
        static let alertMessageSuccess = "Your money is being sent and should be available in 2 days."
        static let alertMessageFailure = "Please make sure all the fields are correctly filled."
    }
    
    
    private let apiClient = ExchangeRateAPIClient()
    
    var rates = Bindable<Rates>()
    var validBinaryValue: Bool = false
    
    
    var countries: [Country]
    var selectedCountry : Country?
    
    init() {
        countries = CountriesData().countries
        selectedCountry = countries[0]
    }
    
    func selectedCountryUpdated(at position: Int) -> Country? {
        selectedCountry = countries[position]
        return selectedCountry
    }
    
    func phoneNumberSizeValidator(length: Int) -> Bool? {
        guard let digitsAfterPrefix = selectedCountry?.digitsAfterPrefix else {
            return nil
        }
        return length <= digitsAfterPrefix
    }
    
    func convertAmount(textField: UITextField) -> String? {
        
        guard let currencyAbbreviation = selectedCountry?.currencyAbbreviation,
              let rate = rates.value?.rates[currencyAbbreviation],
              let binaryValue = textField.text else {
            fatalError("Could not load rates")
        }
        
        guard let intValue = Int(binaryValue, radix: 2) else {
            return nil
        }
        let exchangedValue = Float(intValue) * rate
        
        if exchangedValue > Float(Int.max) {
            validBinaryValue = false
            return Constants.maxValueWarn
        }else{
            validBinaryValue = true
            return String(Int(exchangedValue), radix: 2)
        }
    }
    
    func validatePhoneNumber(text: String) -> Bool {
        
        guard let selectedCountry = selectedCountry else {
            return false
        }

        if text.count < selectedCountry.digitsAfterPrefix {
            return false
        }else if text.isEmpty {
            return false
        }else{
            return true
        }
    }
    
    func validateBinaryInput(input: String) -> Bool{
        if input.isEmpty {
            return true
        }else{
            let regex = "[0-1]"
            return NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: input)
        }
    }
    
    func updateRates(){
        apiClient.fetch { result in
        
            switch result {
            case .success(let rates):
                self.rates.value = rates
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    
}

