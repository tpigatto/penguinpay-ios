//
//  Rates.swift
//  PenguinPay
//
//  Created by Tiago Pigatto Lenza on 25/11/21.
//

import Foundation

struct Rates : Codable {
    
    var base: String
    var timestamp: Int
    var rates: [String:Float]
    
}
