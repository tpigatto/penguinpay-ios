//
//  ViewController.swift
//  PenguinPay
//
//  Created by Tiago Pigatto Lenza on 25/11/21.
//

import UIKit

class SendPaymentViewController: UIViewController {
    
    var uiController: SendPaymentView
    var viewModel = SendPaymentViewModel()
    var rates: Rates?
    let apiClient = ExchangeRateAPIClient()
    
    
    init() {
        uiController = SendPaymentView()
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = uiController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupCountryPicker()
        setupSendPaymentViewModel()
        viewModel.updateRates()
    }
    
    
    fileprivate func setupSendPaymentViewModel(){
        viewModel.rates.bind { [weak self] (rates) in
            DispatchQueue.main.async {
                self?.rates = rates
            }
        }
    }
    
    func setupView(){
        uiController.amountToSendTextField.delegate = self
        uiController.phoneNumberTextField.delegate = self
        uiController.fnTextField.delegate = self
        uiController.lnTextField.delegate = self
        uiController.countryCodeTextField.delegate = self
        uiController.delegate = self
    }
    
    func setupCountryPicker(){
        let picker = UIPickerView()
        picker.delegate = self
        picker.dataSource = self
        uiController.countryCodeTextField.inputView = picker
    }
    
    func clearTextFields(with country: Country) {
        uiController.countryCodeTextField.text = country.flagAndDigits
        uiController.phoneNumberTextField.text = ""
        uiController.amountToSendTextField.text = ""
        uiController.amountToReceiveLabel.text = SendPaymentViewModel.Constants.receiveValueText
    }
    
    func validateInputs() -> Bool{
        if uiController.fnTextField.text!.isEmpty ||
            uiController.lnTextField.text!.isEmpty ||
            !viewModel.validatePhoneNumber(text: uiController.phoneNumberTextField.text!) ||
            uiController.amountToSendTextField.text!.isEmpty ||
            !viewModel.validBinaryValue ||
            viewModel.selectedCountry == nil {
            return false
        }else{
            return true
        }
    }
    
    func presentAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension SendPaymentViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == uiController.fnTextField {
            uiController.lnTextField.becomeFirstResponder()
        }else if textField == uiController.lnTextField {
            uiController.countryCodeTextField.becomeFirstResponder()
        }
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let text = textField.text else {
            return false
        }
        if textField == uiController.phoneNumberTextField {
            let maxLength = text.count + string.count - range.length
            guard let sizeValidator = viewModel.phoneNumberSizeValidator(length: maxLength) else {
                return false
            }
            return sizeValidator
        }else if textField == uiController.amountToSendTextField {
            return viewModel.validateBinaryInput(input: string)
        }
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        if textField == uiController.amountToSendTextField {
            guard let currencyAbbreviation = viewModel.selectedCountry?.currencyAbbreviation,
                  let convertedAmount = viewModel.convertAmount(textField: textField) else {
                      self.uiController.amountToReceiveLabel.text = SendPaymentViewModel.Constants.receiveValueText
                      return
                  }
            DispatchQueue.main.async {
                self.uiController.amountToReceiveLabel.text = SendPaymentViewModel.Constants.receiveValueText + " " + convertedAmount + " " +  currencyAbbreviation
            }
            
        }
    }
    
}

extension SendPaymentViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return viewModel.countries[row].flagAndDigits
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let country = viewModel.selectedCountryUpdated(at: row) else {
            return
        }
        clearTextFields(with: country)
    }
}

extension SendPaymentViewController : SendPaymentViewDelegate {
    func sendMoneyTapped() {
        if validateInputs(){
            presentAlert(title: SendPaymentViewModel.Constants.alertTitleSuccess, message: SendPaymentViewModel.Constants.alertMessageSuccess)
        }else{
            presentAlert(title: SendPaymentViewModel.Constants.alertTitleFailure, message: SendPaymentViewModel.Constants.alertMessageFailure)
        }
    }
}

