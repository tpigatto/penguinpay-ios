//
//  CodeView.swift
//  PenguinPay
//
//  Created by Tiago Pigatto Lenza on 25/11/21.
//

import Foundation

protocol CodeViewProtocol {
    
    func setupViewHierarchy()
    func setupConstraints()
    func setupAdditionalConfiguration()
    
}

extension CodeViewProtocol {
    
    func setupView(){
        setupViewHierarchy()
        setupConstraints()
        setupAdditionalConfiguration()
    }
    
}
