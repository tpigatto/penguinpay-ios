//
//  SendPaymentView.swift
//  PenguinPay
//
//  Created by Tiago Pigatto Lenza on 25/11/21.
//

import UIKit

protocol SendPaymentViewDelegate : AnyObject {
    
    func sendMoneyTapped()
}

class SendPaymentView: UIView {
    
    weak var delegate : SendPaymentViewDelegate?
    
    lazy var layoutContainer: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .vertical
        view.distribution = .fill
        view.spacing = 0.0
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        return view
    }()
    
    lazy var nameLabel: UILabel = {
        var view = UILabel(frame: .zero)
        view.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        view.text = "Recipient:"
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var nameTextFieldContainer: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .horizontal
        view.distribution = .fillEqually
        view.spacing = 8.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var fnTextField: UITextField = {
        let view = UITextField(frame: .zero)
        view.placeholder = "First Name"
        view.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        view.borderStyle = .roundedRect
        view.keyboardType = .default
        view.returnKeyType = .next
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var lnTextField: UITextField = {
        let view = UITextField(frame: .zero)
        view.placeholder = "Last Name"
        view.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        view.borderStyle = .roundedRect
        view.keyboardType = .default
        view.returnKeyType = .next
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var phoneNumberLabelContainer: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .horizontal
        view.distribution = .fillProportionally
        view.spacing = 8.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var phoneNumberLabel: UILabel = {
        var view = UILabel(frame: .zero)
        view.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        view.text = "Phone Number:"
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var countryCodeLabel: UILabel = {
        var view = UILabel(frame: .zero)
        view.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        view.text = "Country:"
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var phoneNumberContainer: UIStackView = {
        let view = UIStackView(frame: .zero)
        view.axis = .horizontal
        view.distribution = .fillProportionally
        view.spacing = 8.0
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var countryCodeTextField: UITextField = {
        let view = UITextField(frame: .zero)
        view.text = "🇰🇪+254"
        view.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        view.borderStyle = .roundedRect
        view.keyboardType = .default
        view.returnKeyType = .next
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var phoneNumberTextField: UITextField = {
        let view = UITextField(frame: .zero)
        view.placeholder = "Phone"
        view.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        view.borderStyle = .roundedRect
        view.keyboardType = .phonePad
        view.returnKeyType = .next
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var amountToSendLabel: UILabel = {
        var view = UILabel(frame: .zero)
        view.font = UIFont.systemFont(ofSize: 12, weight: .semibold)
        view.text = "Amount to send (USD):"
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var amountToSendTextField: UITextField = {
        let view = UITextField(frame: .zero)
        view.placeholder = "Amount"
        view.font = UIFont.systemFont(ofSize: 15, weight: .regular)
        view.borderStyle = .roundedRect
        view.keyboardType = .numberPad
        view.returnKeyType = .next
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var amountToReceiveLabel: UILabel = {
        var view = UILabel(frame: .zero)
        view.font = UIFont.systemFont(ofSize: 17, weight: .medium)
        view.textColor = UIColor.red.withAlphaComponent(0.8)
        view.numberOfLines = 0
        view.text = "Your friend will receive: "
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var sendButton: UIButton = {
        var view = UIButton(frame: .zero)
        view.setTitle("SEND", for: .normal)
        view.setTitleColor(.white, for: .normal)
        view.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        view.backgroundColor = .systemGreen
        view.translatesAutoresizingMaskIntoConstraints = false
        view.addTarget(self, action: #selector(sendMoney), for: .touchUpInside)
        return view
    }()
    
    @objc func sendMoney() {
        self.delegate?.sendMoneyTapped()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

extension SendPaymentView : CodeViewProtocol {
    
    func setupViewHierarchy() {
        layoutContainer.addArrangedSubview(nameLabel)
        
        nameTextFieldContainer.addArrangedSubview(fnTextField)
        nameTextFieldContainer.addArrangedSubview(lnTextField)
        layoutContainer.addArrangedSubview(nameTextFieldContainer)
        
        
        phoneNumberLabelContainer.addArrangedSubview(countryCodeLabel)
        phoneNumberLabelContainer.addArrangedSubview(phoneNumberLabel)
        layoutContainer.addArrangedSubview(phoneNumberLabelContainer)
        
        phoneNumberContainer.addArrangedSubview(countryCodeTextField)
        phoneNumberContainer.addArrangedSubview(phoneNumberTextField)
        layoutContainer.addArrangedSubview(phoneNumberContainer)
        
        layoutContainer.addArrangedSubview(amountToSendLabel)
        layoutContainer.addArrangedSubview(amountToSendTextField)
        layoutContainer.addArrangedSubview(amountToReceiveLabel)
        layoutContainer.addArrangedSubview(sendButton)
        
        
        addSubview(layoutContainer)
        
    }
    
    func setupConstraints() {
        //Constraints for Container StackView
        NSLayoutConstraint.activate([
            layoutContainer.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8.0),
            layoutContainer.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8.0),
            layoutContainer.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 8.0),
        ])
        
        //Constraints for nameTextFieldContainer StackView
        NSLayoutConstraint.activate([
            nameTextFieldContainer.leadingAnchor.constraint(equalTo: nameTextFieldContainer.superview!.leadingAnchor, constant: 0.0),
            nameTextFieldContainer.trailingAnchor.constraint(equalTo: nameTextFieldContainer.superview!.trailingAnchor, constant: 0.0)
        ])
        
        //Constraints for phoneNumberLabelContainer StackView
        NSLayoutConstraint.activate([
            phoneNumberLabelContainer.leadingAnchor.constraint(equalTo: phoneNumberLabelContainer.superview!.leadingAnchor, constant: 0.0),
            phoneNumberLabelContainer.trailingAnchor.constraint(equalTo: phoneNumberLabelContainer.superview!.trailingAnchor, constant: 0.0),
            countryCodeLabel.widthAnchor.constraint(equalToConstant: 80)
        ])
        
        //Constraints for phoneNumberContainer StackView
        NSLayoutConstraint.activate([
            phoneNumberContainer.leadingAnchor.constraint(equalTo: phoneNumberContainer.superview!.leadingAnchor, constant: 0.0),
            phoneNumberContainer.trailingAnchor.constraint(equalTo: phoneNumberContainer.superview!.trailingAnchor, constant: 0.0),
            countryCodeTextField.widthAnchor.constraint(equalToConstant: 80)
        ])
        
        NSLayoutConstraint.activate([
            sendButton.heightAnchor.constraint(equalToConstant: 50)
        ])
        
    }
    
    func setupAdditionalConfiguration() {
        self.backgroundColor = .white
        layoutContainer.setCustomSpacing(15.0, after: nameTextFieldContainer)
        layoutContainer.setCustomSpacing(40.0, after: phoneNumberContainer)
        layoutContainer.setCustomSpacing(15.0, after: amountToSendTextField)
        layoutContainer.setCustomSpacing(15.0, after: amountToReceiveLabel)
        sendButton.layer.cornerRadius = 3.0
    }
    
    
}
